FROM nginx:latest
COPY nginx.conf /etc/nginx/nginx.conf 

COPY ./web/*.html /usr/share/nginx/html/
COPY ./web/css/*.css /usr/share/nginx/html/
COPY ./web/css/main.css /usr/share/nginx/html/
COPY ./web/assets/*.png /usr/share/nginx/html/
COPY ./web/assets/qoutenobg.png /usr/share/nginx/html/
COPY ./web/js/*.js /usr/share/nginx/html/
